from django.db import models


# One
class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    created = models.DateTimeField(null=True)

    def __str__(self):
        return f"{self.title}"


# Many
class Comment(models.Model):
    # Fields/Columns
    author = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, null=True)
    content = models.TextField(max_length=1000)
    # Sets up a One-to-Many relationship
    post = models.ForeignKey(
        "Post", related_name="comments", on_delete=models.CASCADE
    )

    def __str__(self):
        return f"Comment by {self.author} on {self.post} {self.post.created}"

class Keyword(models.Model):
    word = models.CharField(max_length=20)
    posts = models.ManyToManyField("Post", related_name="keywords")
